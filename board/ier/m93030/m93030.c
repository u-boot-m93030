	/*
	 * (C) Copyright 2002
	 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
	 * Marius Groeger <mgroeger@sysgo.de>
	 *
	 * See file CREDITS for list of people who contributed to this
	 * project.
	 *
	 * This program is free software; you can redistribute it and/or
	 * modify it under the terms of the GNU General Public License as
	 * published by the Free Software Foundation; either version 2 of
	 * the License, or (at your option) any later version.
	 *
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 * GNU General Public License for more details.
	 *
	 * You should have received a copy of the GNU General Public License
	 * along with this program; if not, write to the Free Software
	 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
	 * MA 02111-1307 USA
	 */

#include <common.h>
#include <dataflash.h>
#include <at91rm9200_net.h>
#include <ks8721.h>
#include <asm/arch/embedded_services.h>
#include <asm/arch/lib_AT91RM9200.h>

AT91PS_TC tmr;

/* the number of clocks per CFG_HZ */
#define TIMER_LOAD_VAL (CFG_HZ_CLOCK/CFG_HZ)

/* macro to read the 16 bit timer */
#define READ_TIMER (tmr->TC_CV & 0x0000ffff)

static ulong timestamp;
static ulong lastinc;
AT91S_RomBoot const *pAT91;
AT91S_CtlTempo         ctlTempo;
void AT91F_ST_ASM_Handler(void);


//*----------------------------------------------------------------------------
//* \fn    AT91F_DBGU_Printk
//* \brief This function is used to send a string through the DBGU channel (Very low level debugging)
//*----------------------------------------------------------------------------
void AT91F_DBGU_Printk(
	char *buffer) // \arg pointer to a string ending by \0
{
	while(*buffer != '\0') {
		while (!AT91F_US_TxReady((AT91PS_USART)AT91C_BASE_DBGU));
		AT91F_US_PutChar((AT91PS_USART)AT91C_BASE_DBGU, *buffer++);
	}
}



//*----------------------------------------------------------------------------
//* \fn    AT91F_DataAbort
//* \brief This function reports an Abort
//*----------------------------------------------------------------------------
void AT91F_SpuriousHandler(void) 
{
        int *ptr=0x21e00000;
	*ptr = 0;
	AT91F_DBGU_Printk("-F- Spurious Interrupt detected\n\r");
	while (1);
}


//*----------------------------------------------------------------------------
//* \fn    AT91F_DataAbort
//* \brief This function reports an Abort
//*----------------------------------------------------------------------------
void AT91F_DataAbort(void) 
{
        int *ptr=0x21e00000;
	*ptr = 1;
	AT91F_DBGU_Printk("-F- Data Abort detected\n\r");
	while (1);
}

//*----------------------------------------------------------------------------
//* \fn    AT91F_FetchAbort
//* \brief This function reports an Abort
//*----------------------------------------------------------------------------
void AT91F_FetchAbort(void)
{
        int *ptr=0x21e00000;
	*ptr = 2;
	AT91F_DBGU_Printk("-F- Prefetch Abort detected\n\r");
	while (1);
}

//*----------------------------------------------------------------------------
//* \fn    AT91F_Undef
//* \brief This function reports an Abort
//*----------------------------------------------------------------------------
void AT91F_Undef(void) 
{
        int *ptr=0x21e00000;
	*ptr = 3;
	AT91F_DBGU_Printk("-F- Undef detected\n\r");
	while (1);
}

//*----------------------------------------------------------------------------
//* \fn    AT91F_UndefHandler
//* \brief This function reports that no handler have been set for current IT
//*----------------------------------------------------------------------------
void AT91F_UndefHandler(void) 
{
        int *ptr=0x21e00000;
	*ptr = 4;
	AT91F_DBGU_Printk("-F- Undef detected\n\r");
	while (1);
}


void AT91F_ST_ASM_Handler(void);

void switch_led(void);

void stop_led(void)
{
  AT91PS_PIO piod = AT91C_BASE_PIOD;
  AT91F_AIC_DisableIt(AT91C_BASE_AIC, AT91C_ID_SYS);
  
  piod->PIO_ODR = AT91C_PIO_PD6;	/* Disables all the output pins */
  piod->PIO_PER = AT91C_PIO_PD6;	/* Enables the PIO to control all the pins */
  piod->PIO_OER = AT91C_PIO_PD6;	/* Enables the PIO to control all the pins */
  piod->PIO_SODR = AT91C_PIO_PD6;	/* Enables the PIO to control all the pins */
}

void turn_led_off(void)
{
  AT91PS_PIO piod = AT91C_BASE_PIOD;
  piod->PIO_ODR = AT91C_PIO_PD6;	/* Disables all the output pins */
  piod->PIO_PER = AT91C_PIO_PD6;	/* Enables the PIO to control all the pins */
  piod->PIO_OER = AT91C_PIO_PD6;	/* Enables the PIO to control all the pins */
  piod->PIO_SODR = AT91C_PIO_PD6;	/* Enables the PIO to control all the pins */
}

void turn_led_on()
{
  AT91PS_PIO piod = AT91C_BASE_PIOD;
  piod->PIO_ODR = AT91C_PIO_PD6;	/* Disables all the output pins */
  piod->PIO_PER = AT91C_PIO_PD6;	/* Enables the PIO to control all the pins */
  piod->PIO_OER = AT91C_PIO_PD6;	/* Enables the PIO to control all the pins */
  piod->PIO_CODR = AT91C_PIO_PD6;	/* Enables the PIO to control all the pins */
}

#define ILIM0 0
#define ILIM1 200
#define ILIM2 400
int cnt=0;
void switch_led()
{
   switch ( cnt)
   {
      case ILIM0 :
      {
         turn_led_on();
	 cnt++;
	 break;
      }

      case ILIM1:
      {
         turn_led_off();
	 cnt++;
	 break;
      }
      case ILIM2:
      {
         cnt = ILIM0;
	 break;
      }
      default:
      {
         cnt++;
	 break;
      }
   }
}


AT91S_CtlTempo         ctlTempo;

void m93030_irq(void)
{
  AT91C_BASE_ST->ST_SR & 0xff; 
  switch_led();
  ctlTempo.CtlTempoTick(&ctlTempo);
  return;
}

void m93030_set_memsize(char *cmd)
{
  char *memst;
  char *newcmd;
  int *pRegister;
  static char bo[512];
  static char bo1[512];

  pRegister = (int *)0xFFFFFF98;
  switch ( *pRegister)
  {
	case SDRC_CR_VAL_32M :
	{
	  memst="mem=32M ";
	  break;
	}
	case SDRC_CR_VAL_64M :
	{
	  memst="mem=64M ";
	  break;
	}
	case SDRC_CR_VAL_128M :
	{
	  memst="mem=128M ";
	  break;
	}
	default:
	{
          memst="mem=64M ";
          break;
	}
  }
  
  memset(bo,0,sizeof(bo));
  memset(bo1,0,sizeof(bo1));
  strcpy(bo,cmd);;
  newcmd = strstr(bo,"mem=");
  if ( newcmd != 0)
  {
     while ( *newcmd != ' ')
     {
        *newcmd = ' ' ;
        newcmd++;
     }
  }
  strcpy(bo1,memst);
  memst = bo;
  while ( *memst == ' ')
    memst++;
  strcat(bo1,memst);
  strcpy(cmd,bo1);
}


void AT91F_ST_Handler(void)
{
  AT91C_BASE_ST->ST_SR & 0xff; 
  switch_led();
  ctlTempo.CtlTempoTick(&ctlTempo);
  return;
}


int board_init (void)
{

  DECLARE_GLOBAL_DATA_PTR;
  AT91PS_AIC Aic;
  AT91PS_PIO at91ps_pio;
  volatile int i=0;

  /* Enable Ctrlc */
  console_init_f();


  /* Correct IRDA resistor problem */
  /* Set PA23_TXD in Output */
  at91ps_pio = (AT91PS_PIO) AT91C_BASE_PIOA->PIO_OER;
  at91ps_pio = AT91C_PA23_TXD2;
  
  /* memory and cpu-speed are setup before relocation */
  /* so we do _nothing_ here */
  
  /* arch number of AT91RM9200DK-Board */
  gd->bd->bi_arch_number = MACH_TYPE_M93030;
  /* adress of boot parameters */
  gd->bd->bi_boot_params = PHYS_SDRAM + 0x100;
  
  return (0);
}

int dram_init (void)
{
	DECLARE_GLOBAL_DATA_PTR;

	gd->bd->bi_dram[0].start = PHYS_SDRAM;
	gd->bd->bi_dram[0].size = PHYS_SDRAM_SIZE;
	return 0;
}

int m93030_interrupt_preinit()
{
  AT91F_AIC_AcknowledgeIt(AT91C_BASE_AIC);
  AT91F_AIC_AcknowledgeIt(AT91C_BASE_AIC);
  AT91F_AIC_AcknowledgeIt(AT91C_BASE_AIC);
  AT91F_AIC_AcknowledgeIt(AT91C_BASE_AIC);
  AT91F_AIC_AcknowledgeIt(AT91C_BASE_AIC);
  AT91F_AIC_AcknowledgeIt(AT91C_BASE_AIC);
  AT91F_AIC_AcknowledgeIt(AT91C_BASE_AIC);
  AT91F_AIC_AcknowledgeIt(AT91C_BASE_AIC);
  
  AT91F_AIC_SetExceptionVector((unsigned int *)0x0C, AT91F_FetchAbort);
  AT91F_AIC_SetExceptionVector((unsigned int *)0x10, AT91F_DataAbort);
  AT91F_AIC_SetExceptionVector((unsigned int *)0x4, AT91F_Undef);
}

int m93030_interrupt_init (void)
{

  /*
  AT91F_AIC_Open(
		 AT91C_BASE_AIC,          // pointer to the AIC registers
		 AT91F_ST_ASM_Handler, // IRQ exception vector
		 AT91F_UndefHandler,      // FIQ exception vector
		 AT91F_UndefHandler,      // AIC default handler
		 AT91F_SpuriousHandler,   // AIC spurious handler
		 0);                      // Protect mode
		 */
  
  // Perform 8 End Of Interrupt Command to make sure AIC will not Lock out nIRQ 
  
  pAT91 = AT91C_ROM_BOOT_ADDRESS;
  
  pAT91->OpenCtlTempo(&ctlTempo, (void *) &(pAT91->SYSTIMER_DESC));
  ctlTempo.CtlTempoStart((void *) &(pAT91->SYSTIMER_DESC));
    
  
  AT91F_AIC_ConfigureIt (
			 AT91C_BASE_AIC,                        // AIC base address
			 AT91C_ID_SYS,                          // System peripheral ID
			 AT91C_AIC_PRIOR_HIGHEST,               // Max priority
			 AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE, // Level sensitive
			 AT91F_ST_ASM_Handler );						
  
  AT91F_AIC_EnableIt(AT91C_BASE_AIC, AT91C_ID_SYS);

  
  return (0);
}


#ifdef CONFIG_DRIVER_ETHER
#if defined(CONFIG_CMD_NET)

/*
 * Name:
 *	at91rm9200_GetPhyInterface
 * Description:
 *	Initialise the interface functions to the PHY
 * Arguments:
 *	None
 * Return value:
 *	None
 */
void at91rm9200_GetPhyInterface(AT91PS_PhyOps p_phyops)
{
	p_phyops->Init = ks8721_InitPhy;
	p_phyops->IsPhyConnected = ks8721_IsPhyConnected;
	p_phyops->GetLinkSpeed = ks8721_GetLinkSpeed;
	p_phyops->AutoNegotiate = ks8721_AutoNegotiate;
}

#endif
#endif


/*
 * Disk On Chip (NAND) Millenium initialization.
 * The NAND lives in the CS2* space
 */
#if (CONFIG_COMMANDS & CFG_CMD_NAND)
extern ulong nand_probe (ulong physadr);

#define AT91_SMARTMEDIA_BASE 0x40000000	/* physical address to access memory on NCS3 */
void nand_init (void)
{
	/* Setup Smart Media, fitst enable the address range of CS3 */
	*AT91C_EBI_CSA |= AT91C_EBI_CS3A_SMC_SmartMedia;
	/* set the bus interface characteristics based on
	   tDS Data Set up Time 30 - ns
	   tDH Data Hold Time 20 - ns
	   tALS ALE Set up Time 20 - ns
	   16ns at 60 MHz ~= 3  */
/*memory mapping structures */
#define SM_ID_RWH	(5 << 28)
#define SM_RWH		(1 << 28)
#define SM_RWS		(0 << 24)
#define SM_TDF		(1 << 8)
#define SM_NWS		(3)
	AT91C_BASE_SMC2->SMC2_CSR[3] = (SM_RWH | SM_RWS |
		AT91C_SMC2_ACSS_STANDARD | AT91C_SMC2_DBW_8 |
		SM_TDF | AT91C_SMC2_WSEN | SM_NWS);

	/* enable the SMOE line PC0=SMCE, A21=CLE, A22=ALE */
	*AT91C_PIOC_ASR = AT91C_PC0_BFCK | AT91C_PC1_BFRDY_SMOE |
		AT91C_PC3_BFBAA_SMWE;
	*AT91C_PIOC_PDR = AT91C_PC0_BFCK | AT91C_PC1_BFRDY_SMOE |
		AT91C_PC3_BFBAA_SMWE;

	/* Configure PC2 as input (signal READY of the SmartMedia) */
	*AT91C_PIOC_PER = AT91C_PC2_BFAVD;	/* enable direct output enable */
	*AT91C_PIOC_ODR = AT91C_PC2_BFAVD;	/* disable output */

	/* Configure PB1 as input (signal Card Detect of the SmartMedia) */
	*AT91C_PIOB_PER = AT91C_PIO_PB1;	/* enable direct output enable */
	*AT91C_PIOB_ODR = AT91C_PIO_PB1;	/* disable output */

	/* PIOB and PIOC clock enabling */
	*AT91C_PMC_PCER = 1 << AT91C_ID_PIOB;
	*AT91C_PMC_PCER = 1 << AT91C_ID_PIOC;

	if (*AT91C_PIOB_PDSR & AT91C_PIO_PB1)
		printf ("  No SmartMedia card inserted\n");
#ifdef DEBUG
	printf ("  SmartMedia card inserted\n");

	printf ("Probing at 0x%.8x\n", AT91_SMARTMEDIA_BASE);
#endif
	printf ("%4lu MB\n", nand_probe(AT91_SMARTMEDIA_BASE) >> 20);
}
#endif
