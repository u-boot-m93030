/*
 * (C) Copyright 2008
 * Ulf Samuelsson <ulf@atmel.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 */
#include <common.h>
#include <config.h>
#include <asm/hardware.h>
#include <dataflash.h>

AT91S_DATAFLASH_INFO dataflash_info[CFG_MAX_DATAFLASH_BANKS];

struct dataflash_addr cs[CFG_MAX_DATAFLASH_BANKS] = {
	{CFG_DATAFLASH_LOGIC_ADDR_CS0, 0},	/* Logical adress, CS */
	{CFG_DATAFLASH_LOGIC_ADDR_CS1, 1},	/* Logical adress, CS */
	{CFG_DATAFLASH_LOGIC_ADDR_CS2, 2},	/* Logical adress, CS */
	{CFG_DATAFLASH_LOGIC_ADDR_CS3, 3}
};

/*define the area offsets*/
dataflash_protect_t area_list[CFG_MAX_DATAFLASH_BANKS][NB_DATAFLASH_AREA] = {
        {
	{P_ROM_START, -1, FLAG_PROTECT_SET,   0, "Bootstrap"},
	{0, 0, 0, 0, "End"},
	{0, 0, 0, 0, ""},
	{0, 0, 0, 0, ""},
	{0, 0, 0, 0, ""}
	},
        {
	{P_U_BOOT_START, P_U_BOOT_END, FLAG_PROTECT_SET,   0, "Bootstrap"},
	{P_U_BOOT_END + 1, P_ENV_END, FLAG_PROTECT_CLEAR,   FLAG_SETENV, "Environment"},
	{P_ENV_END + 1 , P_KERNEL_END, FLAG_PROTECT_CLEAR, 0,	"Kernel"},
	{P_KERNEL_END + 1 , -1, FLAG_PROTECT_CLEAR, 0,	"FS"}
	},
        {
	{0, -1, FLAG_PROTECT_SET,   2, "Free"},
	},
        {
	{0, -1, FLAG_PROTECT_SET,   3, "Free"},
	}
};
