/*
 * NOTE:	Micrel ethernet Physical layer
 *
 * Version:	@(#)KS8721.h	1.0.0	01/10/2001
 *
 * Authors:	ATMEL Rousset
 *
 *
 *		This program is free software; you can redistribute it and/or
 *		modify it under the terms of the GNU General Public License
 *		as published by the Free Software Foundation; either version
 *		2 of the License, or (at your option) any later version.
 */


/* Micrel PHYSICAL LAYER TRANSCEIVER KS8721 */

#define	KS8721_BMCR 		0	/* Basic Mode Control Register */
#define KS8721_BMSR		1	/* Basic Mode Status Register */
#define KS8721_PHYID1		2	/* PHY Idendifier Register 1 */
#define KS8721_PHYID2		3	/* PHY Idendifier Register 2 */
#define KS8721_ANAR		4	/* Auto_Negotiation Advertisement Register  */
#define KS8721_ANLPAR		5	/* Auto_negotiation Link Partner Ability Register */
#define KS8721_ANER		6	/* Auto-negotiation Expansion Register  */
#define KS8721_ANNPR		7	/* Auto-negociation Next Page Register */
#define KS8721_LPNPA		8	/* Link Partner Next Page Ability */

#define KS8721_RXERCR		21	/* RXER Counter Register */
#define KS8721_MDINTR		27	/* Interrupt Control/Status Register */
#define KS8721_100TXPCR		31	/* 100BaseTX Phy Control Register */


/* --Bit definitions: KS8721_BMCR */
#define KS8721_RESET   	         (1 << 15)	/* 1= Software Reset; 0=Normal Operation */
#define KS8721_LOOPBACK	         (1 << 14)	/* 1=loopback Enabled; 0=Normal Operation */
#define KS8721_SPEED_SELECT      (1 << 13)	/* 1=100Mbps; 0=10Mbps */
#define KS8721_AUTONEG	         (1 << 12)
#define KS8721_POWER_DOWN        (1 << 11)
#define KS8721_ISOLATE           (1 << 10)
#define KS8721_RESTART_AUTONEG   (1 << 9)
#define KS8721_DUPLEX_MODE       (1 << 8)
#define KS8721_COLLISION_TEST    (1 << 7)
#define KS8721_DIS_TRANSMITTER	 (1 << 0)

/*--Bit definitions: KS8721_BMSR */
#define KS8721_100BASE_T4        (1 << 15)
#define KS8721_100BASE_TX_FD     (1 << 14)
#define KS8721_100BASE_T4_HD     (1 << 13)
#define KS8721_10BASE_T_FD       (1 << 12)
#define KS8721_10BASE_T_HD       (1 << 11)
#define KS8721_MF_PREAMB_SUPPR   (1 << 6)
#define KS8721_AUTONEG_COMP      (1 << 5)
#define KS8721_REMOTE_FAULT      (1 << 4)
#define KS8721_AUTONEG_ABILITY   (1 << 3)
#define KS8721_LINK_STATUS       (1 << 2)
#define KS8721_JABBER_DETECT     (1 << 1)
#define KS8721_EXTEND_CAPAB      (1 << 0)

/*--definitions: KS8721_PHYID1 */
#define KS8721_PHYID1_OUI	 0x0885
#define KS8721_LSB_MASK	         0x3F

/*--Bit definitions: KS8721_ANAR, KS8721_ANLPAR */
#define KS8721_NP               (1 << 15)
#define KS8721_ACK              (1 << 14)
#define KS8721_RF               (1 << 13)
#define KS8721_FCS              (1 << 10)
#define KS8721_T4               (1 << 9)
#define KS8721_TX_FDX           (1 << 8)
#define KS8721_TX_HDX           (1 << 7)
#define KS8721_10_FDX           (1 << 6)
#define KS8721_10_HDX           (1 << 5)
#define KS8721_AN_IEEE_802_3	0x0001

/*--Bit definitions: KS8721_ANER */
#define KS8721_PDF              (1 << 4)
#define KS8721_LP_NP_ABLE       (1 << 3)
#define KS8721_NP_ABLE          (1 << 2)
#define KS8721_PAGE_RX          (1 << 1)
#define KS8721_LP_AN_ABLE       (1 << 0)

/*--Bit definitions: KS8721_DSCR */
#define KS8721_BP4B5B           (1 << 15)
#define KS8721_BP_SCR           (1 << 14)
#define KS8721_BP_ALIGN         (1 << 13)
#define KS8721_BP_ADPOK         (1 << 12)
#define KS8721_REPEATER         (1 << 11)
#define KS8721_TX               (1 << 10)
#define KS8721_RMII_ENABLE      (1 << 8)
#define KS8721_F_LINK_100       (1 << 7)
#define KS8721_SPLED_CTL        (1 << 6)
#define KS8721_COLLED_CTL       (1 << 5)
#define KS8721_RPDCTR_EN        (1 << 4)
#define KS8721_SM_RST           (1 << 3)
#define KS8721_MFP SC           (1 << 2)
#define KS8721_SLEEP            (1 << 1)
#define KS8721_RLOUT            (1 << 0)

/*--Bit definitions: KS8721_10BTCSR */
#define KS8721_LP_EN           (1 << 14)
#define KS8721_HBE             (1 << 13)
#define KS8721_SQUELCH         (1 << 12)
#define KS8721_JABEN           (1 << 11)
#define KS8721_10BT_SER        (1 << 10)
#define KS8721_POLR            (1 << 0)


/*--Bit definitions: KS8721_MDINTR */
#define KS8721_JABBER_UM	(1 << 15)
#define KS8721_RX_ERR_UM	(1 << 14)
#define KS8721_PG_RX_UM		(1 << 13)
#define KS8721_PAR_ERR_UM	(1 << 12)
#define KS8721_LP_ERR_UM	(1 << 11)
#define KS8721_LINK_DOWN_ERR_UM	(1 << 10)
#define KS8721_RF_ERR_UM	(1 << 9)
#define KS8721_LINK_UP_ERR_UM	(1 << 8)



/******************  function prototypes **********************/
unsigned int  ks8721_IsPhyConnected(AT91PS_EMAC p_mac);
unsigned char ks8721_GetLinkSpeed(AT91PS_EMAC p_mac);
unsigned char ks8721_AutoNegotiate(AT91PS_EMAC p_mac, int *status);
unsigned char ks8721_InitPhy(AT91PS_EMAC p_mac);
