/*
 * Rick Bronson <rick@efn.org>
 *
 * Configuation settings for the AT91RM9200DK board.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CONFIG_H
#define __CONFIG_H

#undef  PHY_ADDR
#define PHY_ADDR		1


/* ARM asynchronous clock */
#define AT91C_MAIN_CLOCK	179712000	/* from 18.432 MHz crystal (18432000 / 4 * 39) */
#define CONFIG_MII
#define AT91C_MASTER_CLOCK	59904000	/* peripheral clock (AT91C_MASTER_CLOCK / 3) */

#define AT91_SLOW_CLOCK		32768	/* slow clock */

#define CONFIG_M93030			/* define if m93030 board	*/
#define CONFIG_ARM920T		1	/* This is an ARM920T Core	*/
#define CONFIG_AT91RM9200	1	/* It's an Atmel AT91RM9200 SoC	*/
#define CONFIG_AT91RM9200DK	1	/* on an AT91RM9200DK Board	*/
#define CONFIG_USE_IRQ			/* we don't need IRQ/FIQ stuff	*/
#define CONFIG_STACKSIZE_IRQ     (4*1024) 
#define CONFIG_STACKSIZE_FIQ     (4*1024) 
#define USE_920T_MMU		1
#undef CONFIG_OF_LIBFDT
#undef CONFIG_FIT

#ifdef CONFIG_USE_IRQ
#define  ARM920_IRQ_CALLBACK m93030_irq
#endif /* USE_IRQ */


#define CONFIG_CMDLINE_TAG	1	/* enable passing of ATAGs	*/
#define CONFIG_SETUP_MEMORY_TAGS 1
#define CONFIG_INITRD_TAG	1

#define P_ROM_START       0
#define P_ROM_SIZE        (32 * 1056 )
#define P_ROM_END         ( P_ROM_START + P_ROM_SIZE - 1)
#define P_U_BOOT_START    (0 )
#define P_U_BOOT_SIZE     ( 152 * 1056)
#define P_U_BOOT_END      (P_U_BOOT_START + P_U_BOOT_SIZE - 1 )
#define P_ENV_START       (P_U_BOOT_END + 1)
#define P_ENV_SIZE        (4 * 1056)
#define P_ENV_END         (P_ENV_START + P_ENV_SIZE - 1)
#define P_KERNEL_START    (P_ENV_END + 1)
#define P_KERNEL_SIZE     ( 2000 * 1056)
#define P_KERNEL_END      (P_KERNEL_START + P_KERNEL_SIZE -1 )
#define P_FS_START        (P_KERNEL_END + 1)
#define P_FS_SIZE         ((1056 * 1024 * 8 ) - (P_ROM_SIZE + P_U_BOOT_SIZE \
			   + P_ENV_SIZE \
			   + P_KERNEL_SIZE))
#define P_FS_END          (P_FS_START + P_FS_SIZE -1 )


#define CONFIG_HARD_I2C         1
#define CFG_I2C_SPEED	400000   /* 400 khz */
#define CFG_I2C_SLAVE	0   /* 400 khz */
#define CONFIG_SKIP_LOWLEVEL_INIT
#define SDRC_CR_VAL_32M		0x2188c155 /* set up the SDRAM */
#define SDRC_CR_VAL_64M		0x2188c159 /* set up the SDRAM */
#define SDRC_CR_VAL_128M	0x2188c15a /* set up the SDRAM */

#ifndef CONFIG_SKIP_LOWLEVEL_INIT
#define CFG_USE_MAIN_OSCILLATOR		1
/* flash */
#define MC_PUIA_VAL	0x00000000
#define MC_PUP_VAL	0x00000000
#define MC_PUER_VAL	0x00000000
#define MC_ASR_VAL	0x00000000
#define MC_AASR_VAL	0x00000000
#define EBI_CFGR_VAL	0x00000000
#define SMC2_CSR_VAL	0x00003284 /* 16bit, 2 TDF, 4 WS */

/* clocks */
#define PLLAR_VAL	0x20263E04 /* 179.712000 MHz for PCK */
#define PLLBR_VAL	0x10483E0E /* 48.054857 MHz (divider by 2 for USB) */
#define MCKR_VAL	0x00000202 /* PCK/3 = MCK Master Clock = 59.904000MHz from PLLA */

/* sdram */
#define PIOC_ASR_VAL	0xFFFF0000 /* Configure PIOC as peripheral (D16/D31) */
#define PIOC_BSR_VAL	0x00000000
#define PIOC_PDR_VAL	0xFFFF0000
#define EBI_CSA_VAL	0x00000002 /* CS1=SDRAM */
#define SDRAM		0x20000000 /* address of the SDRAM */
#define SDRAM1		0x20000080 /* address of the SDRAM */
#define SDRAM_VAL	0x00000000 /* value written to SDRAM */
#define SDRC_MR_VAL	0x00000002 /* Precharge All */
#define SDRC_MR_VAL1	0x00000004 /* refresh */
#define SDRC_MR_VAL2	0x00000003 /* Load Mode Register */
#define SDRC_MR_VAL3	0x00000000 /* Normal Mode */
#define SDRC_TR_VAL	0x000002E0 /* Write refresh rate */
#endif	/* CONFIG_SKIP_LOWLEVEL_INIT */
/*
 * Size of malloc() pool
 */
#define CFG_MALLOC_LEN	((((CFG_ENV_SIZE + 1024) - (CFG_ENV_SIZE % 1024))  + 128*1024))
#define CFG_GBL_DATA_SIZE	128	/* size in bytes reserved for initial data */

#define CONFIG_BAUDRATE 115200

#define CFG_AT91C_BRGR_DIVISOR	33	/* hardcode so no __divsi3 : AT91C_MASTER_CLOCK / baudrate / 16 */

/*
 * Hardware drivers
 */

/* define one of these to choose the DBGU, USART0  or USART1 as console */
#define CONFIG_DBGU
#undef CONFIG_USART0
#undef CONFIG_USART1

#define	 CONFIG_HWFLOW			/* don't include RTS/CTS flow control support	*/

#undef	CONFIG_MODEM_SUPPORT		/* disable modem initialization stuff */


#define CFG_USB_OHCI_SLOT_NAME		"m93030"
#define CFG_USB_OHCI_MAX_ROOT_PORTS	2

#define CFG_USB_OHCI_REGS_BASE AT91_USB_HOST_BASE
#define CONFIG_BOOTDELAY      3
#define CONFIG_ENV_OVERWRITE	1
#define CONFIG_JFFS2_CMDLINE

#include <config_cmd_default.h>
#undef  CONFIG_CMD_AUTOSCRIPT   /* Autoscript Support           */
#define CONFIG_CMD_BOOTD        /* bootd                        */
#define CONFIG_CMD_CONSOLE      /* coninfo                      */
#define CONFIG_CMD_ECHO         /* echo arguments               */
#define CONFIG_CMD_ENV          /* saveenv                      */
#define CONFIG_CMD_FLASH        /* flinfo, erase, protect       */
#undef CONFIG_CMD_FPGA         /* FPGA configuration Support   */
#undef CONFIG_CMD_IMI          /* iminfo                       */
#define CONFIG_CMD_IMLS         /* List all found images        */
#undef CONFIG_CMD_ITEST        /* Integer (and string) test    */
#define CONFIG_CMD_LOADB        /* loadb                        */
#define CONFIG_CMD_LOADS        /* loads                        */
#define CONFIG_CMD_MEMORY       /* md mm nm mw cp cmp crc base loop mtest */
#define CONFIG_CMD_MISC         /* Misc functions like sleep etc*/
#define CONFIG_CMD_NET          /* bootp, tftpboot, rarpboot    */
#undef CONFIG_CMD_NFS          /* NFS support                  */
#undef CONFIG_CMD_RUN          /* run command in env variable  */
#undef CONFIG_CMD_SETGETDCR    /* DCR support on 4xx           */
#undef CONFIG_CMD_XIMG         /* Load part of Multi Image     */

#define CONFIG_CMD_NET
#define CONFIG_CMD_MII
#define CONFIG_CMD_DHCP
#define CONFIG_CMD_I2C
#define CONFIG_CMD_BDI
#define CONFIG_CMD_PING


#define CONFIG_USB_OHCI 1
#define CONFIG_USB_OHCI_NEW 1
#define CFG_USB_OHCI_CPU_INIT 1
#define CONFIG_CMD_USB 1
#define CONFIG_USB_STORAGE 1
#define CONFIG_USB_OHCI 1
#undef CONFIG_ISO_PARTITION 1
#define CONFIG_DOS_PARTITION 1


#undef CONFIG_CMD_LOADS
#undef CONFIG_CMD_LOADB
#undef CONFIG_CMD_LOADY
#undef CONFIG_CMD_NFS
#define CONFIG_CMD_FLASH
#define CONFIG_CMD_JFFS2

#undef CONFIG_CMD_IMI
#undef CONFIG_CMD_AUTOSCRIPT
#undef CONFIG_CMD_FPGA
#undef CONFIG_CMD_MISC

#define CONFIG_BOOTARGS "root=/dev/ram0 rw console=ttyS0,115200 mem=128M mtdparts=AT91_Concatenated_Flash:0x8400(romboot),0x837c00(free),0x27300(u-boot),0x1080(env),0x203a00(kernel),-(F1) firstboot ramdisk_size=32000"
/* this must be included AFTER the definition of CONFIG_COMMANDS (if any) */

#define CFG_MAX_NAND_DEVICE	1	/* Max number of NAND devices		*/
#define SECTORSIZE 512

#define ADDR_COLUMN 1
#define ADDR_PAGE 2
#define ADDR_COLUMN_PAGE 3

#define NAND_ChipID_UNKNOWN	0x00
#define NAND_MAX_FLOORS 1
#define NAND_MAX_CHIPS 1

#define AT91_SMART_MEDIA_ALE (1 << 22)	/* our ALE is AD22 */
#define AT91_SMART_MEDIA_CLE (1 << 21)	/* our CLE is AD21 */

#define NAND_DISABLE_CE(nand) do { *AT91C_PIOC_SODR = AT91C_PIO_PC0;} while(0)
#define NAND_ENABLE_CE(nand) do { *AT91C_PIOC_CODR = AT91C_PIO_PC0;} while(0)

#define NAND_WAIT_READY(nand) while (!(*AT91C_PIOC_PDSR & AT91C_PIO_PC2))

#define WRITE_NAND_COMMAND(d, adr) do{ *(volatile __u8 *)((unsigned long)adr | AT91_SMART_MEDIA_CLE) = (__u8)(d); } while(0)
#define WRITE_NAND_ADDRESS(d, adr) do{ *(volatile __u8 *)((unsigned long)adr | AT91_SMART_MEDIA_ALE) = (__u8)(d); } while(0)
#define WRITE_NAND(d, adr) do{ *(volatile __u8 *)((unsigned long)adr) = (__u8)d; } while(0)
#define READ_NAND(adr) ((volatile unsigned char)(*(volatile __u8 *)(unsigned long)adr))
/* the following are NOP's in our implementation */
#define NAND_CTL_CLRALE(nandptr)
#define NAND_CTL_SETALE(nandptr)
#define NAND_CTL_CLRCLE(nandptr)
#define NAND_CTL_SETCLE(nandptr)

#define CONFIG_NR_DRAM_BANKS 1
#define PHYS_SDRAM 0x20000000
#define PHYS_SDRAM_SIZE 0x4000000  /* 64 megs */

#define CFG_MEMTEST_START		PHYS_SDRAM
#define CFG_MEMTEST_END			CFG_MEMTEST_START + PHYS_SDRAM_SIZE - 262144

#define CONFIG_DRIVER_ETHER
#define CONFIG_NET_RETRY_COUNT		20
#define CONFIG_AT91C_USE_RMII

#define CONFIG_HAS_DATAFLASH		1
#define CFG_SPI_WRITE_TOUT		(5*CFG_HZ)
#define CFG_MAX_DATAFLASH_BANKS 	4
#define CFG_MAX_DATAFLASH_PAGES 	8192
#define CFG_DATAFLASH_LOGIC_ADDR_CS0	0xC0000000	/* Logical adress for CS0 */
#define CFG_DATAFLASH_LOGIC_ADDR_CS1	0xC1000000	/* Logical adress for CS0 */
#define CFG_DATAFLASH_LOGIC_ADDR_CS2	0xC2000000	/* Logical adress for CS0 */
#define CFG_DATAFLASH_LOGIC_ADDR_CS3	0xC3000000	/* Logical adress for CS0 */

#define PHYS_FLASH_1			0x10000000
#define PHYS_FLASH_SIZE			0x200000  /* 2 megs main flash */
#define CFG_FLASH_BASE			PHYS_FLASH_1
#define CFG_MAX_FLASH_BANKS		1
#define CFG_MAX_FLASH_SECT		256
#define CFG_FLASH_ERASE_TOUT		(2*CFG_HZ) /* Timeout for Flash Erase */
#define CFG_FLASH_WRITE_TOUT		(2*CFG_HZ) /* Timeout for Flash Write */

#define	CFG_ENV_IS_IN_DATAFLASH

#ifdef CFG_ENV_IS_IN_DATAFLASH
#define CFG_ENV_OFFSET			(P_ENV_START)
#define CFG_ENV_ADDR			(CFG_DATAFLASH_LOGIC_ADDR_CS1 + CFG_ENV_OFFSET)
#define CFG_ENV_SIZE			0x1080  /* 0x8000 */
#else
#define CFG_ENV_IS_IN_FLASH		1
#ifndef CONFIG_SKIP_LOWLEVEL_INIT
#define CFG_ENV_ADDR			(PHYS_FLASH_1 + 0x60000)  /* after u-boot.bin */
#define CFG_ENV_SIZE			0x10000 /* sectors are 64K here */
#else
#define CFG_ENV_ADDR			(PHYS_FLASH_1 + 0xe000)  /* between boot.bin and u-boot.bin.gz */
#define CFG_ENV_SIZE			0x2000  /* 0x8000 */
#endif	/* CONFIG_SKIP_LOWLEVEL_INIT */
#endif	/* CFG_ENV_IS_IN_DATAFLASH */


#define CFG_LOAD_ADDR		0x21000000  /* default load address */

#ifndef CONFIG_SKIP_LOWLEVEL_INIT
#define CFG_BOOT_SIZE		0x00 /* 0 KBytes */
#define CFG_U_BOOT_BASE		PHYS_FLASH_1
#define CFG_U_BOOT_SIZE		0x60000 /* 384 KBytes */
#else
#define CFG_BOOT_SIZE		0x6000 /* 24 KBytes */
#define CFG_U_BOOT_BASE		(PHYS_FLASH_1 + 0x10000)
#define CFG_U_BOOT_SIZE		0x10000 /* 64 KBytes */
#endif	/* CONFIG_SKIP_LOWLEVEL_INIT */

#define CONFIG_BOOTCOMMAND      "saveenv \; usb start \; usbboot \;  bootm 0xC1028380 \; bootp \; bootm " 

#define CFG_BAUDRATE_TABLE	{115200 , 19200, 38400, 57600, 9600 }

#define CFG_PROMPT		"U-Boot> "	/* Monitor Command Prompt */
#define CFG_CBSIZE		256		/* Console I/O Buffer Size */
#define CFG_MAXARGS		16		/* max number of command args */
#define CFG_PBSIZE		(CFG_CBSIZE+sizeof(CFG_PROMPT)+16) /* Print Buffer Size */

#ifndef __ASSEMBLY__
/*-----------------------------------------------------------------------
 * Board specific extension for bd_info
 *
 * This structure is embedded in the global bd_info (bd_t) structure
 * and can be used by the board specific code (eg board/...)
 */

struct bd_info_ext {
	/* helper variable for board environment handling
	 *
	 * env_crc_valid == 0    =>   uninitialised
	 * env_crc_valid  > 0    =>   environment crc in flash is valid
	 * env_crc_valid  < 0    =>   environment crc in flash is invalid
	 */
	int env_crc_valid;
};
#endif

#define CFG_HZ 1000
#define CFG_HZ_CLOCK AT91C_MASTER_CLOCK/2	/* AT91C_TC0_CMR is implicitly set to */
					/* AT91C_TC_TIMER_DIV1_CLOCK */

#define CONFIG_STACKSIZE	(32*1024)	/* regular stack */

#define CONFIG_ETHADDR	00:04:E8:01:86:9F

/*
#ifdef CONFIG_USE_IRQ
#error CONFIG_USE_IRQ not supported
#endif
*/
#define CFG_LONGHELP

#endif
