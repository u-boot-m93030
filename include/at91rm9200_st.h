/* ---------------------------------------------------------------------------- */
/*	    ATMEL Microcontroller Software Support  -  ROUSSET	-		*/
/* ---------------------------------------------------------------------------- */
/*  The software is delivered "AS IS" without warranty or condition of any	*/
/*  kind, either express, implied or statutory. This includes without		*/
/*  limitation any warranty or condition with respect to merchantability or	*/
/*  fitness for any particular purpose, or against the infringements of		*/
/*  intellectual property rights of others.					*/
/* ---------------------------------------------------------------------------- */
/* File Name	       : at91rm9200_i2c.h					*/
/* Object	       : AT91RM9200 / TWI definitions				*/
/* Generated	       : AT91 SW Application Group  12/03/2002 (10:48:02)	*/
/*										*/
/* ---------------------------------------------------------------------------- */

#ifndef AT91RM9200_ST_H
#define AT91RM9200_ST_H

#ifndef __ASSEMBLY__
// *****************************************************************************
//              SOFTWARE API DEFINITION  FOR System Timer Interface
// *****************************************************************************
typedef struct _AT91S_ST {
	AT91_REG	 ST_CR; 	// Control Register
	AT91_REG	 ST_PIMR; 	// Period Interval Mode Register
	AT91_REG	 ST_WDMR; 	// Watchdog Mode Register
	AT91_REG	 ST_RTMR; 	// Real-time Mode Register
	AT91_REG	 ST_SR; 	// Status Register
	AT91_REG	 ST_IER; 	// Interrupt Enable Register
	AT91_REG	 ST_IDR; 	// Interrupt Disable Register
	AT91_REG	 ST_IMR; 	// Interrupt Mask Register
	AT91_REG	 ST_RTAR; 	// Real-time Alarm Register
	AT91_REG	 ST_CRTR; 	// Current Real-time Register
} AT91S_ST, *AT91PS_ST;

// -------- ST_CR : (ST Offset: 0x0) System Timer Control Register -------- 
#define AT91C_ST_WDRST        ((unsigned int) 0x1 <<  0) // (ST) Watchdog Timer Restart
// -------- ST_PIMR : (ST Offset: 0x4) System Timer Period Interval Mode Register -------- 
#define AT91C_ST_PIV          ((unsigned int) 0xFFFF <<  0) // (ST) Watchdog Timer Restart
// -------- ST_WDMR : (ST Offset: 0x8) System Timer Watchdog Mode Register -------- 
#define AT91C_ST_WDV          ((unsigned int) 0xFFFF <<  0) // (ST) Watchdog Timer Restart
#define AT91C_ST_RSTEN        ((unsigned int) 0x1 << 16) // (ST) Reset Enable
#define AT91C_ST_EXTEN        ((unsigned int) 0x1 << 17) // (ST) External Signal Assertion Enable
// -------- ST_RTMR : (ST Offset: 0xc) System Timer Real-time Mode Register -------- 
#define AT91C_ST_RTPRES       ((unsigned int) 0xFFFF <<  0) // (ST) Real-time Timer Prescaler Value
// -------- ST_SR : (ST Offset: 0x10) System Timer Status Register -------- 
#define AT91C_ST_PITS         ((unsigned int) 0x1 <<  0) // (ST) Period Interval Timer Interrupt
#define AT91C_ST_WDOVF        ((unsigned int) 0x1 <<  1) // (ST) Watchdog Overflow
#define AT91C_ST_RTTINC       ((unsigned int) 0x1 <<  2) // (ST) Real-time Timer Increment
#define AT91C_ST_ALMS         ((unsigned int) 0x1 <<  3) // (ST) Alarm Status
// -------- ST_IER : (ST Offset: 0x14) System Timer Interrupt Enable Register -------- 
// -------- ST_IDR : (ST Offset: 0x18) System Timer Interrupt Disable Register -------- 
// -------- ST_IMR : (ST Offset: 0x1c) System Timer Interrupt Mask Register -------- 
// -------- ST_RTAR : (ST Offset: 0x20) System Timer Real-time Alarm Register -------- 
#define AT91C_ST_ALMV         ((unsigned int) 0xFFFFF <<  0) // (ST) Alarm Value Value
// -------- ST_CRTR : (ST Offset: 0x24) System Timer Current Real-time Register -------- 
#define AT91C_ST_CRTV         ((unsigned int) 0xFFFFF <<  0) // (ST) Current Real-time Value


#endif	/* __ASSEMBLY__ */
#endif	/* AT91RM9200_TWI_H */
