/* ---------------------------------------------------------------------------- */
/*	    ATMEL Microcontroller Software Support  -  ROUSSET	-		*/
/* ---------------------------------------------------------------------------- */
/*  The software is delivered "AS IS" without warranty or condition of any	*/
/*  kind, either express, implied or statutory. This includes without		*/
/*  limitation any warranty or condition with respect to merchantability or	*/
/*  fitness for any particular purpose, or against the infringements of		*/
/*  intellectual property rights of others.					*/
/* ---------------------------------------------------------------------------- */
/* File Name	       : at91rm9200_i2c.h					*/
/* Object	       : AT91RM9200 / TWI definitions				*/
/* Generated	       : AT91 SW Application Group  12/03/2002 (10:48:02)	*/
/*										*/
/* ---------------------------------------------------------------------------- */

#ifndef AT91RM9200_MC_H
#define AT91RM9200_MC_H

#ifndef __ASSEMBLY__
// *****************************************************************************
//              SOFTWARE API DEFINITION  FOR Memory Controller Interface
// *****************************************************************************
typedef struct _AT91S_MC {
	AT91_REG	 MC_RCR; 	// MC Remap Control Register
	AT91_REG	 MC_ASR; 	// MC Abort Status Register
	AT91_REG	 MC_AASR; 	// MC Abort Address Status Register
	AT91_REG	 Reserved0[1]; 	// 
	AT91_REG	 MC_PUIA[16]; 	// MC Protection Unit Area
	AT91_REG	 MC_PUP; 	// MC Protection Unit Peripherals
	AT91_REG	 MC_PUER; 	// MC Protection Unit Enable Register
} AT91S_MC, *AT91PS_MC;

// -------- MC_RCR : (MC Offset: 0x0) MC Remap Control Register -------- 
#define AT91C_MC_RCB          ((unsigned int) 0x1 <<  0) // (MC) Remap Command Bit
// -------- MC_ASR : (MC Offset: 0x4) MC Abort Status Register -------- 
#define AT91C_MC_UNDADD       ((unsigned int) 0x1 <<  0) // (MC) Undefined Addess Abort Status
#define AT91C_MC_MISADD       ((unsigned int) 0x1 <<  1) // (MC) Misaligned Addess Abort Status
#define AT91C_MC_MPU          ((unsigned int) 0x1 <<  2) // (MC) Memory protection Unit Abort Status
#define AT91C_MC_ABTSZ        ((unsigned int) 0x3 <<  8) // (MC) Abort Size Status
#define 	AT91C_MC_ABTSZ_BYTE                 ((unsigned int) 0x0 <<  8) // (MC) Byte
#define 	AT91C_MC_ABTSZ_HWORD                ((unsigned int) 0x1 <<  8) // (MC) Half-word
#define 	AT91C_MC_ABTSZ_WORD                 ((unsigned int) 0x2 <<  8) // (MC) Word
#define AT91C_MC_ABTTYP       ((unsigned int) 0x3 << 10) // (MC) Abort Type Status
#define 	AT91C_MC_ABTTYP_DATAR                ((unsigned int) 0x0 << 10) // (MC) Data Read
#define 	AT91C_MC_ABTTYP_DATAW                ((unsigned int) 0x1 << 10) // (MC) Data Write
#define 	AT91C_MC_ABTTYP_FETCH                ((unsigned int) 0x2 << 10) // (MC) Code Fetch
#define AT91C_MC_MST0         ((unsigned int) 0x1 << 16) // (MC) Master 0 Abort Source
#define AT91C_MC_MST1         ((unsigned int) 0x1 << 17) // (MC) Master 1 Abort Source
#define AT91C_MC_SVMST0       ((unsigned int) 0x1 << 24) // (MC) Saved Master 0 Abort Source
#define AT91C_MC_SVMST1       ((unsigned int) 0x1 << 25) // (MC) Saved Master 1 Abort Source
// -------- MC_PUIA : (MC Offset: 0x10) MC Protection Unit Area -------- 
#define AT91C_MC_PROT         ((unsigned int) 0x3 <<  0) // (MC) Protection
#define 	AT91C_MC_PROT_PNAUNA               ((unsigned int) 0x0) // (MC) Privilege: No Access, User: No Access
#define 	AT91C_MC_PROT_PRWUNA               ((unsigned int) 0x1) // (MC) Privilege: Read/Write, User: No Access
#define 	AT91C_MC_PROT_PRWURO               ((unsigned int) 0x2) // (MC) Privilege: Read/Write, User: Read Only
#define 	AT91C_MC_PROT_PRWURW               ((unsigned int) 0x3) // (MC) Privilege: Read/Write, User: Read/Write
#define AT91C_MC_SIZE         ((unsigned int) 0xF <<  4) // (MC) Internal Area Size
#define 	AT91C_MC_SIZE_1KB                  ((unsigned int) 0x0 <<  4) // (MC) Area size 1KByte
#define 	AT91C_MC_SIZE_2KB                  ((unsigned int) 0x1 <<  4) // (MC) Area size 2KByte
#define 	AT91C_MC_SIZE_4KB                  ((unsigned int) 0x2 <<  4) // (MC) Area size 4KByte
#define 	AT91C_MC_SIZE_8KB                  ((unsigned int) 0x3 <<  4) // (MC) Area size 8KByte
#define 	AT91C_MC_SIZE_16KB                 ((unsigned int) 0x4 <<  4) // (MC) Area size 16KByte
#define 	AT91C_MC_SIZE_32KB                 ((unsigned int) 0x5 <<  4) // (MC) Area size 32KByte
#define 	AT91C_MC_SIZE_64KB                 ((unsigned int) 0x6 <<  4) // (MC) Area size 64KByte
#define 	AT91C_MC_SIZE_128KB                ((unsigned int) 0x7 <<  4) // (MC) Area size 128KByte
#define 	AT91C_MC_SIZE_256KB                ((unsigned int) 0x8 <<  4) // (MC) Area size 256KByte
#define 	AT91C_MC_SIZE_512KB                ((unsigned int) 0x9 <<  4) // (MC) Area size 512KByte
#define 	AT91C_MC_SIZE_1MB                  ((unsigned int) 0xA <<  4) // (MC) Area size 1MByte
#define 	AT91C_MC_SIZE_2MB                  ((unsigned int) 0xB <<  4) // (MC) Area size 2MByte
#define 	AT91C_MC_SIZE_4MB                  ((unsigned int) 0xC <<  4) // (MC) Area size 4MByte
#define 	AT91C_MC_SIZE_8MB                  ((unsigned int) 0xD <<  4) // (MC) Area size 8MByte
#define 	AT91C_MC_SIZE_16MB                 ((unsigned int) 0xE <<  4) // (MC) Area size 16MByte
#define 	AT91C_MC_SIZE_64MB                 ((unsigned int) 0xF <<  4) // (MC) Area size 64MByte
#define AT91C_MC_BA           ((unsigned int) 0x3FFFF << 10) // (MC) Internal Area Base Address
// -------- MC_PUP : (MC Offset: 0x50) MC Protection Unit Peripheral -------- 
// -------- MC_PUER : (MC Offset: 0x54) MC Protection Unit Area -------- 
#define AT91C_MC_PUEB         ((unsigned int) 0x1 <<  0) // (MC) Protection Unit enable Bit



#endif	/* __ASSEMBLY__ */
#endif	/* AT91RM9200_TWI_H */
